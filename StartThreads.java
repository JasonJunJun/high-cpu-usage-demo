/**
 * start it all
 *
 * @author srasul
 */
public class StartThreads {

    public static void main(String[] args) {
        // lets start 1 heavy ...
        HeavyTask heavyTask = new HeavyTask(1000);
        new Thread(heavyTask).start();

        // ... and 3 light threads
        LightTask lightTask = new LightTask();
        new Thread(lightTask).start();
        new Thread(lightTask).start();
        new Thread(lightTask).start();
    }
}