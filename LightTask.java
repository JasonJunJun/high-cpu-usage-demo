import java.util.Random;

/**
 * task that does little work. just count & sleep
 *
 * @author srasul
 */
public class LightTask implements Runnable {

    @Override
    public void run() {
        long l = 0L;
        //noinspection InfiniteLoopStatement
        while (true) {
            l++;
            try {
                //noinspection BusyWait
                Thread.sleep(new Random().nextInt(10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (l == Long.MAX_VALUE) {
                l = 0L;
            }
        }
    }
}
