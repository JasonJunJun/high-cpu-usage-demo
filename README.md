1. 首先运行`javac *.java`编译所有.java文件，然后运行`java StartThreads &`启动程序。

2. 运行`top`获取到high CPU usage进程的ID，这里定义变量`{high_CPU_usage_process_id}`为那个进程的ID。
3. 运行`top -H -p {high_CPU_usage_process_id}`获取到high CPU usage进程中具体线程的信息，假设有个线程造成了high CPU usage，这里定义变量`{high_CPU_usage_thread_id}`为那个线程的PID。
4. 运行`jstack {high_CPU_usage_process_id} > stackTrace.txt`导出进程的stack dump。
5. 将`{high_CPU_usage_thread_id}`转换为HEX格式，假设`{high_CPU_usage_thread_id}`为308204，那么HEX格式就是`0x4B3EC`。执行`grep -A 100 -i '0x4B3EC' stackTrace.txt`查看这个线程具体的stack trace。通过stack trace可以得知线程一直循环执行CPU密集型指令（计算MD5 hashes），这就是造成high CPU usage的原因。

## References

* [multithreading - High CPU Utilization in java application - why? - Stack Overflow0](https://stackoverflow.com/questions/15811411/high-cpu-utilization-in-java-application-why)
* [Identifying which Java Thread is consuming most CPU | Nomad Labs Code](http://web.archive.org/web/20190403090107/http://code.nomad-labs.com/2010/11/18/identifying-which-java-thread-is-consuming-most-cpu/)

