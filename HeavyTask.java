import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * task that does some heavy lifting
 *
 * @author srasul
 */
public class HeavyTask implements Runnable {

    private final long length;

    public HeavyTask(long length) {
        this.length = length;
    }

    @Override
    public void run() {
        //noinspection InfiniteLoopStatement
        while (true) {
            StringBuilder data = new StringBuilder();

            // make some shit up
            for (int i = 0; i < length; i++) {
                data.append(UUID.randomUUID());
            }

            MessageDigest digest;
            try {
                digest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }

            // hash that shit
            digest.update(data.toString().getBytes());
        }
    }
}